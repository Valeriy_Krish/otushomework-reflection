﻿
namespace Reflection;

static class Program
{
    private static bool _flag =true;

    static void Main(string[] args)
    {
        Start();
    }

    public static void Start()
    {
        var service = new TestService();
        while (_flag)
        {
            Console.WriteLine("Выберите команду, введите значение от 0 до 2:\n" +
                "0. Exit\n" +
                "1. Кастомная сереализация/десериализация (формат CSV)\n" +
                "2. Библиотека Newtonsoft.Json\n");

            var command = Console.ReadLine();
            Console.WriteLine("-------------------------------------------");

            switch (command)
            {
                case "0": _flag = false; break;
                case "1": service.TestCsv(); break;
                case "2": service.TestJson(); break;
                default: Console.WriteLine("Неизвестная команда\n"); break;
            }
        }
    }


}
