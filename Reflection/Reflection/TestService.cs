﻿using System.Diagnostics;
using System.Text;
using Newtonsoft.Json;
using Reflection.Infrastructure.Service;

namespace Reflection
{
    internal sealed class TestService
    {
        private readonly  F _model = new();
        private readonly  Stopwatch _timer = new();
        private readonly  SerealizeService _serealizeService = new();
        public void TestCsv()
        {
            var obj = _model.GetData();
            
            Console.WriteLine($"Тест -> Кастомная сериализация (100 итераций) -> Время:{Test(100, () => _serealizeService.Serealize<F>(obj))}\n");
            Console.WriteLine($"Тест -> Кастомная сериализация (100000 итераций) -> Время:{Test(100000, () => _serealizeService.Serealize<F>(obj))}\n");

            var fileName = CreateFile(_serealizeService.Serealize<F>(obj), true);
            string result = String.Empty;
            using (StreamReader file = File.OpenText(fileName))
            {
                result = file.ReadToEnd();
            }

            Console.WriteLine($"Тест -> Кастомная десериализация (100 итераций) -> Время:{Test(100, () => _serealizeService.Deserealize<F>(result))}\n");
            Console.WriteLine($"Тест -> Кастомная десериализация (100000 итераций) -> Время:{Test(100000, () => _serealizeService.Deserealize<F>(result))}\n");
           
            Console.WriteLine("-------------------------------------------");

        }
        public void TestJson()
        {            
            Console.WriteLine($"Тест ->  Сериализация Newtonsoft.Json (100 итераций) -> Время:{Test(100, () => JsonConvert.SerializeObject(_model.GetData()))}\n");
            Console.WriteLine($"Тест ->  Сериализация Newtonsoft.Json (100000 итераций) -> Время:{Test(100000, () => JsonConvert.SerializeObject(_model.GetData()))}\n");

            var fileName = CreateFile(JsonConvert.SerializeObject(_model.GetData()), false);
            string result = String.Empty;
            using (StreamReader file = File.OpenText(fileName))
            {
                result = file.ReadToEnd();
            }

            Console.WriteLine($"Тест ->  Десериализация Newtonsoft.Json (100 итераций) -> Время:{Test(100, () => JsonConvert.DeserializeObject<F>(result))}\n");
            Console.WriteLine($"Тест ->  Десериализация Newtonsoft.Json (100000 итераций) -> Время:{Test(100000, () => JsonConvert.DeserializeObject<F>(result))}\n");
            
            Console.WriteLine("-------------------------------------------");
        }
        private string Test<T>(int n, Func<T> func)
        {
            _timer.Start();
            for (int i =0; i<n; i++)
            {
                func();
            }
            _timer.Stop();
            return GetElapsedTime(_timer.Elapsed);
        }

        private static string GetElapsedTime(TimeSpan ts)
        {
            return string.Format("{0:00}.{1:00}",ts.Seconds,ts.Milliseconds);
        }

        private static string CreateFile(string text, bool flag) 
        {
            string fileName; 

            if (flag)
                fileName = "Test.csv";
            else
                fileName = "Test.json";

            fileName = Path.Combine(Path.GetFullPath(AppDomain.CurrentDomain.BaseDirectory), fileName);

            using (var fileStr = new FileStream(fileName, FileMode.OpenOrCreate))
            {
                byte[] buffer = Encoding.Default.GetBytes(text);
                fileStr.WriteAsync(buffer, 0, buffer.Length).Wait();
                Console.WriteLine($"Текст записан в файл {fileName}\n");
            }
            return fileName;
        }

    }
    public sealed class F
    {
        public int i1; 
        public int i2;
        public int i3;
        public int i4;
        public int i5;
        public F GetData() => new () { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
    }
}
