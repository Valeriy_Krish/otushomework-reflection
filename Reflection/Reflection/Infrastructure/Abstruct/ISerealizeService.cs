﻿using System.Collections.Generic;

namespace Reflection.Infrastructure.Abstruct
{
    internal interface ISerealizeService
    {
        public string Serealize<T>(T obj);
        public T Deserealize<T>(string obj) where T : class, new();
    }
}
