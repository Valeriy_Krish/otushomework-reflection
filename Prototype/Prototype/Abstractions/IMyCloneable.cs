﻿
namespace Prototype.Abstractions
{
    internal interface IMyCloneable<T>
    {
        T MyClone();
    }
}
