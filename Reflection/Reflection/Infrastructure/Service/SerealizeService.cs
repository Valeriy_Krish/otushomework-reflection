﻿using Reflection.Infrastructure.Abstruct;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Reflection.Infrastructure.Service
{
    internal class SerealizeService:ISerealizeService
    {
        public string Serealize<T>(T obj )
        {
            if (obj is null)
                throw new ArgumentException();

            var delimetr = ";";
            var str = new StringBuilder();

            var myType = typeof(T).GetFields();

            var data = new List<string>();
            foreach (var prop in myType)
            {
                var propValue = prop.GetValue(obj);
                data.Add(propValue == null ? "" : propValue.ToString());
            }
            return str.Append(string.Join(delimetr, data)).ToString();
        }

        public T Deserealize<T>(string obj) where T : class, new()
        {
            if(obj is null)
                throw new ArgumentException();

            var delimetr = ";";
            FieldInfo[] fields = typeof(T).GetFields();
            PropertyInfo[] propertyInfos = typeof(T).GetProperties();

            var values = obj.Split(delimetr);
            var data = new T();

            for (int i = 0; i < fields.Length; i++)
            {
                fields[i].SetValue(data, Convert.ChangeType(values[i], fields[i].FieldType));
            }
            return data;
        }
    }
}
