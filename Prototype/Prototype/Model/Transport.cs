﻿
namespace Prototype.Model
{
    public class Transport
    {
        /// <summary>
        /// Объем двигателя
        /// </summary>
        public int EngineVolume { get; set; }

        /// <summary>
        /// Мощность 
        /// </summary>
        public int EnginePower { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; } = "Transport";

        /// <summary>
        /// Вид транспорта
        /// </summary>
        public string KindOfTransport { get; set; }

        public Transport() { }

        public Transport(int engineVolume, int enginePower, string name, string kindOfTransport)
        {
            EngineVolume = engineVolume;
            EnginePower = enginePower;
            Name = name;
            KindOfTransport = kindOfTransport;
        }

        public Transport(Transport transport)
        {
            EngineVolume = transport.EngineVolume;
            EnginePower = transport.EnginePower;
            Name = transport.Name;
            KindOfTransport = transport.KindOfTransport;
        }

        public virtual string Move() => "Транспорт отправился в путь";

        public object Clone() => MyClone();

        public virtual Transport MyClone() => new(this);

        public override string ToString() =>
            $"EngineVolume: {EngineVolume} | EnginePower: {EnginePower} | Name: {Name} | KindOfTransport: {KindOfTransport}, {Move()}";

    }
}
