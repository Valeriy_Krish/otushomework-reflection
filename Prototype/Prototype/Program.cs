﻿using Faker;
using Prototype.Model;

namespace Prototype;

public sealed class Program
{
    static void Main(string[] args)
    {
        var transport = new Transport(2,200, NameFaker.FirstName(), "наземный");
        var car = new Car(2, 200, NameFaker.FirstName(), "наземный", false);
        var moto = new Motorcycle(2,100, NameFaker.FirstName(), "наземный",true);
        var quad = new HondaQuad(2,100 , NameFaker.FirstName(), "наземный" , false , true);

        CheckClone<Transport>(transport);
        CheckClone<Car>(car);
        CheckClone<Motorcycle>(moto);
        CheckClone<HondaQuad>(quad);

    }
    private static void CheckClone<T>(Transport transport) where T : Transport
    {
        string type = typeof(T).Name;
        Console.WriteLine($"\nCheck Class [{type}]\n");
        var cloneTransport1 = (T)transport.Clone();
        var cloneTransport2 = transport.MyClone();
        PrintPets(type, transport, cloneTransport1, cloneTransport2);

        Console.WriteLine("\nChanges..");
        transport.Name = "Honda-CRV";
        cloneTransport1.Name = "HondaStreet";
        cloneTransport2.Name = "HondaMoto";
        PrintPets(type, transport, cloneTransport1, cloneTransport2);
    }

    private static void PrintPets<T>(string type, T origin, T clone1, T clone2) where T : Transport
    {
        Console.WriteLine($"[{type}] origin: {origin}\n" +
                          $"[{type}] clone1: {clone1}\n" +
                          $"[{type}] clone2: {clone2}");
    }

}
